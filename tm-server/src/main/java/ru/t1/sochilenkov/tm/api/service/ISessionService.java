package ru.t1.sochilenkov.tm.api.service;

import ru.t1.sochilenkov.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {

}
