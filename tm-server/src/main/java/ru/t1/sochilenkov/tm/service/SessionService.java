package ru.t1.sochilenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.sochilenkov.tm.api.repository.ISessionRepository;
import ru.t1.sochilenkov.tm.api.service.ISessionService;
import ru.t1.sochilenkov.tm.model.Session;

public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull ISessionRepository repository) {
        super(repository);
    }

}
